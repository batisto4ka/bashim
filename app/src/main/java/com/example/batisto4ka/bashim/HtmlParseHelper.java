package com.example.batisto4ka.bashim;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;


/**
 * Created by batisto4ka on 30.09.2014.
 * Class for parsing page of site www.bash.im
 * @author Natali Makarenko
 */
public class HtmlParseHelper {
    /**
     * quote expression for parse
     */
    public static final String QUOTE=".quote";
    /**
     * image expression for parse
     */
    public static final String IMAGE="img";

    public HtmlParseHelper () {
    }

    /**
     * Connection to URL
     * @param url -  url to connect
     * @return doc - Document object with page in html format
     */
    public Document connect(String url) {
        Document doc=null;
        try{
            doc = Jsoup.connect(url).get();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    /**
     * Get certain data (quotes) from Document
     * @param doc - Document object with page in html format
     * @param field - type of data to parse @see HtmlParseHelper.QUOTE, HtmlParseHelper.IMAGE
     * @return - Quote object with parsed data
     */
    public Quote getData(Document doc,final String field)
    {
        Elements       wholeQuote=doc.select(field);
        Quote rQuote=new Quote(wholeQuote.size());
        Elements       id,text=null,comics,rating;

        int i=0;
        for (Element datas: wholeQuote)
        {
            rating=datas.select("[class=rating]");
            id=datas.select("a[class=id]");
            if (id.size()==0)
                id=datas.select("[class=id]");
            text=datas.select(".text");
            comics=datas.select("a[class=comics]");
            if ((text.size()!=0)&&(text.hasText())) {
                if (rating.size() != 0)
                    rQuote.setRating(i, rating.text());
                if (id.size() != 0)
                    rQuote.setId(i, id.text());
                rQuote.setText(i, text.text());
                if (comics.size() != 0)
                    rQuote.setComicsRef(i, comics.attr("href"));
                else rQuote.setComicsRef(i, null);

                i++;
            }
        }
        rQuote.size=i-1;
        Elements page=doc.select("[class=page]");
        if (page.size()!=0) {
            rQuote.setCurPageNum(page.get(0).attr("value"));
            rQuote.setMaxPageNum(Integer.valueOf(page.get(0).attr("max")));
        }
        else
        {
            rQuote.setCurPageNum("0");
            rQuote.setMaxPageNum(0);
        }

        return rQuote;
    }

    /**
     * Get comics image Url
     * @param doc - Document object with page in html format
     * @param field - type of data to parse @see HtmlParseHelper.QUOTE, HtmlParseHelper.IMAGE
     * @return - comics url
     */
    public String getImage(Document doc,final String field)
    {
        String result=null;
        try {
            Elements image = doc.select(field);
            Elements el = image.select("[id=cm_strip]");
            result = el.attr("src");
        }catch (Exception e){}
        return result;
    }
}
