package com.example.batisto4ka.bashim;

/**
 * Created by batisto4ka on 07.10.2014.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A placeholder fragment containing a listView with page content
 * @author Natali Slinko
 */
public class PlaceHolderFragment extends Fragment implements View.OnClickListener {
    /**
     * Log for read/write SharedPreferences file
     */
    public static final String LOG_SPREF="LOG_SHARED_PREFERENCES";
    /**
     * Log string value
     */
    public static final String LOG="LOG";
    /**
     * Log error
     */
    public static final String LOG_ERROR_MSG="Error";
    /**
     * Intent string value for image source
     */
    public static final String INTENT_IMAGE_SRC="src";
    /**
     * Bundle string value for quote object
     */
    public static final String BUNDLE_QUOTE="QUOTE";
    /**
     * Bundle string value for ListView instance state
     */
    public static final String BUNDLE_LV_STATE="LV_STATE";
    /**
     * Bundle string value for loaderState
     */
    public static final String BUNDLE_LOADER_STATE="loaderState";

    /**
     * Indication of user's action for Load images
     */
    public static final byte IMAGE_LOAD = 0;
    /**
     * Indication of user's action for Showing images
     */
    public static final byte IMAGE_SHOW = 1;
    /**
     * Indication of user's action when user clicks rulez
     */
    public static final byte RULEZ = 1;
    /**
     * Indication of user's action when user clicks sux
     */
    public static final byte SUX = 2;
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    /**
     * List view that displays quotes
     */
    private ListView lv;
    /**
     * Object of class PageNavigator that implements page navigation logics
     */
     public PageNavigator pN;
    /**
     * Buttons previous page, next page
     */
    Button btnLeft, btnRight;
    /**
     * Current page
     */
    TextView curPage;
    /**
     * Quotes
     */
    Quote CurrentPage;
    /**
     * Database
     */
    DB db;

    /**
     * pager Btn prev,next, text current page
     */
    public RelativeLayout pager;

    /**
     * progress bar
     */
    public RelativeLayout loader;
    /**
     * loader state
     */
    boolean loaderState;

    public PlaceHolderFragment() {
        loaderState=false;
    }

    /**
     * Configure database adapter
     * @return
     */
    BashDBSimpleCursorAdapter getDBAdapter(Cursor cursor){
        return new BashDBSimpleCursorAdapter(cursor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.page_content, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boolean isConnected;
        InitFragmentChilds();
        pN=new PageNavigator();
        pN.Load();
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null)
            isConnected = activeNetwork.isConnectedOrConnecting();
            else isConnected=false;
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            try{
               if (isConnected) {
                   CurrentPage = new Quote(savedInstanceState.getBundle(BUNDLE_QUOTE));//restore listview content
                   lv.setAdapter(new QuoteAdapter(CurrentPage));
                   lv.onRestoreInstanceState(savedInstanceState.getParcelable(BUNDLE_LV_STATE));//restore listview position
                   restorePager(String.valueOf(pN.getCurrentPageNumber()), pN.getMaxPageNumber());
                   restoreLoaderState(savedInstanceState.getBoolean(BUNDLE_LOADER_STATE));
               }
                else{
                   try {
                       restorePager(String.valueOf(pN.getCurrentPageNumber()), pN.getMaxPageNumber());
                       ReadFromDB(String.valueOf(pN.currentPageNumber),pN.getPageType());
                   }catch(Exception e){Log.e(LOG_SPREF,LOG_ERROR_MSG);}
                   restoreLoaderState(false);
                   Toast.makeText(getActivity(),getResources().getString(R.string.noconnect),Toast.LENGTH_LONG);
                   }
            }catch (Exception e){Log.e(LOG,LOG_ERROR_MSG);}

        }
        else
        {
            CurrentPage = new Quote(1);
            restorePager(String.valueOf(pN.getCurrentPageNumber()), pN.getMaxPageNumber());
            if(!ReadFromDB())
            {//if no last seeing data in db
                if (isConnected)//if network is responsible
                    ShowPage(pN.NEW_SPAGE, pN.CATEGORY_TYPE, pN.getPageType()/*titleNum*/,"0");//load new page
                 else {//no network, no data
                    restoreLoaderState(false);
                    Toast.makeText(getActivity(),getResources().getString(R.string.noconnect),Toast.LENGTH_LONG);
                }
            }
        }

    }

    /**
     * Check last seen data in database by page category type and cach index, show it in listView
     * @return - if last loaded data exists in database
     */
    boolean ReadFromDB(){
        boolean result=false;
        db = new DB(getActivity());
        if (db.MainTable.CheckDataByIndex(DBCach.MAX_INDEX,pN.getPageType()))//check last seeing data in db
        {
            lv.setAdapter(getDBAdapter(db.MainTable.ShowTable(DBCach.MAX_INDEX,pN.getPageType() )));
            ShowPage(pN.getPageStringByNumber(pN.getPageType()), pN.CATEGORY_TYPE, pN.getPageType(),"0");//update loaded page
            result=true;
        }
        db.close();
        return result;
    }

    /**
     * Read data from database by page number and page type and show it in listView
     * @param page - String value of page number to read
     * @param type - page category type @see:PageNavigator
     */
    void ReadFromDB(String page,byte type){
        db = new DB(getActivity());
        if (page==null)
            page="0";

        int pageNumber=Integer.valueOf(page);
        if (db.MainTable.CheckData(type,pageNumber))
        {
            lv.setAdapter(getDBAdapter(db.MainTable.ShowTable(type,pageNumber)));
            pN.setPageType(type);
            restorePager(page,Integer.valueOf(getMaxPageNumber(type)));
        }

        db.close();
    }

    /**
     * Restore pager
     * @param cPage -current number of page
     * @param maxPage - max page number
     */
    void restorePager(String cPage,int maxPage){
    if ((maxPage==0)||(cPage.equals("0")))
    {
       pager.setVisibility(View.GONE);
    }
    else {
            pager.setVisibility(View.VISIBLE);
            curPage.setText(cPage);
            pN.setCurrentPageNumber(Integer.valueOf(cPage));
        }
    }

    /**
     * Restore loader
     * @param state
     */
    void restoreLoaderState(boolean state)
    {
        if (state)//if loading
            loader.setVisibility(View.VISIBLE);
            else loader.setVisibility(View.GONE);
       loaderState=state;
    }
    /**
     * Get loader state
     */
    boolean getLoaderState(){
        return loaderState;
    }

    /**
     * Init fragment layout childs
     */
    void InitFragmentChilds(){
        btnLeft = (Button) getView().findViewById(R.id.leftBtn);
        lv = (ListView) getView().findViewById(R.id.listViewData);
        btnLeft = (Button) getView().findViewById(R.id.leftBtn);
        btnLeft.setOnClickListener(this);
        curPage = (TextView) getView().findViewById(R.id.tvPage);
        btnRight = (Button) getView().findViewById(R.id.rightBtn);
        btnRight.setOnClickListener(this);
        pager=(RelativeLayout) getView().findViewById(R.id.pager);
        loader=(RelativeLayout)getView().findViewById(R.id.loader);
    }
    /**
     * Show Page Method - method that loads and shows page
     *
     * @param page - String name of base page
     * @param operationType - type of page action: update(or navigate to next/prev) or category (new, byrating, random etc.)
     */

    public void ShowPage(String page, byte operationType,byte type,String pageNumber) {
        restoreLoaderState(true);
        if (pageNumber!=null)
            ReadFromDB(pageNumber,type);
        if (operationType == pN.CATEGORY_TYPE)
            pN.LoadCatPage(pN.BASE_PAGE, page,type);
        else if (operationType == pN.UPDATE_TYPE)
            pN.LoadPage(page,type);

    }
    /**
     * Method that calls after getting broadcast receiver intent with loaded parsed data
     * updates buttons and number of current loaded page
     * and save data to database
      @param q - Quote object with parsed data
     */
    public void onDataLoaded(Quote q) {
        restoreLoaderState(false);//hide loader
        try {
            restorePager(q.getCurPageNum(),q.getMaxPageNum());
            if (pN.getPageType()==PageNavigator.NEW_PAGE)
                pN.setNewsPageMaxNumber(String.valueOf(q.getMaxPageNum()));
            setTitle(pN.getPageType());
            CurrentPage = q;
            lv.setAdapter(new QuoteAdapter(CurrentPage));
        }catch (Exception e){
            Toast.makeText(this.getActivity(),R.string.error,Toast.LENGTH_LONG).show();}
    }

    /**
     * Set options menu item current category
     * @param position - position of selected item from navigationDrawer listView with page categories
     */
     @SuppressLint("NewApi")
     public void setTitle(int position) {
        String[] menuItemName = getResources().getStringArray(R.array.menu_item_names);
         Main.activity.mTitle=menuItemName[position];
         (getActivity()).invalidateOptionsMenu();

    }
    /**
     *Method that calls after getting broadcast receiver intent when no data was loaded
     */
    public void onDataLoaded() {
        restoreLoaderState(false);
        Toast.makeText(getActivity(),getResources().getString(R.string.noconnect),Toast.LENGTH_LONG).show();
    }

    /**
     * Method calls after getting broadcast receiver intent, when image loaded or must be shown
     *
     * @param type - operation type: load image source or show image
     * @param src - String value with image source
     */
    public void onDataLoaded(byte type, String src) {
        restoreLoaderState(false);
        if (type == IMAGE_LOAD) {
            pN.LoadImage(pN.BASE_PAGE + src);
        } else {
            Intent intent = new Intent(getActivity(), ImageActivity.class);
            intent.putExtra(INTENT_IMAGE_SRC, src);
            pN.restorePage();
            startActivity(intent);
        }
    }

    /**
     * Method, that changes rating when click on rulez or sux for interface
     *
     * @param position - position of clicked listView item
     * @param rate - rate to change: 1- increment, -1- decrement
     */
    public void ChangeRating(int position, byte rate) {
        try {
            String rating = CurrentPage.getRating()[position];
            rating = String.valueOf(Integer.valueOf(rating) + rate);
            CurrentPage.setRating(position, rating);
            Parcelable state = lv.onSaveInstanceState();
            lv.setAdapter(new QuoteAdapter(CurrentPage));
            lv.onRestoreInstanceState(state);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that calls POST query for change ration of quote
     * @param position - listView item position
     * @param rate - rate value to change on
     */
    public void onPost(int position, byte rate){
        ChangeRating(position,rate);
    }
    /**
     * Method that calls when getting broadcast receiver intent with POST query result
     *
     * @param tag - operation to post: rulez or sux
     * @param pos - id of quote
     * @param position - position in listView adapter of selected quote
     */
    public void onPost(int tag, String pos, int position) {
        try {
            pos = pos.replace("#", "");
            if (tag == RULEZ) {
                pN.LikeQuote("/quote/" + pos + "/rulez", pos, "rulez",position,(byte)1);
             } else if (tag == SUX) {
                pN.LikeQuote("/quote/" + pos + "/sux", pos, "sux",position,(byte)(-1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that returns String with maximum page number for page category type signed in position
     * @param position - number of page category type
     * @return - maximum page number if news page,
     *         - "1" if  byrating page
     *         - "0" if other page
     */
    public String getMaxPageNumber(int position){
    String result="0";
    switch  (position) {
        case PageNavigator.NEW_PAGE:
            result = pN.getNewsPageMaxNumber();
            break;
        case PageNavigator.RATING_PAGE:
            result = "1";
            break;
        }
        return result;
    }
    /**
     * Method onClick for buttons of the page
     * @param view
     */
    @Override
    public void onClick(View view) {
        if(!loaderState) {
            switch (view.getId()) {
                case R.id.leftBtn:
                    if (CurrentPage != null) {
                        if (pN.getPageType()==PageNavigator.NEW_PAGE)
                            ShowPage(pN.BASE_PAGE + pN.getBasePage() + "/" + String.valueOf(pN.getCurrentPageNumber() + 1), pN.UPDATE_TYPE,pN.getPageType(),String.valueOf(pN.getCurrentPageNumber() + 1));//pN.LoadPage(pN.BASE_PAGE+pN.GetBasePage() + "/"+ String.valueOf(cPage+1));
                        else
                            ShowPage(pN.BASE_PAGE + pN.getBasePage() + "/" + String.valueOf(pN.getCurrentPageNumber() - 1), pN.UPDATE_TYPE,pN.getPageType(),String.valueOf(pN.getCurrentPageNumber() - 1));//pN.LoadPage(pN.BASE_PAGE+pN.GetBasePage() + "/"+ String.valueOf(cPage-1));
                    }

                break;
                case R.id.rightBtn:
                    if (CurrentPage != null) {
                        if (pN.getPageType()==PageNavigator.NEW_PAGE)
                            ShowPage(pN.BASE_PAGE + pN.getBasePage() + "/" + String.valueOf(pN.getCurrentPageNumber() - 1), pN.UPDATE_TYPE,pN.getPageType(),String.valueOf(pN.getCurrentPageNumber() - 1));
                        else
                            ShowPage(pN.BASE_PAGE + pN.getBasePage() + "/" + String.valueOf(pN.getCurrentPageNumber() + 1), pN.UPDATE_TYPE,pN.getPageType(),String.valueOf(pN.getCurrentPageNumber() + 1));
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Save instance state of fragment
     * @param outState - Bundle object to save instance state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        pN.Save();
        try {
            outState.putBundle(BUNDLE_QUOTE, CurrentPage.toBundle());
            outState.putParcelable(BUNDLE_LV_STATE, lv.onSaveInstanceState());
            outState.putBoolean(BUNDLE_LOADER_STATE,getLoaderState());
        }catch (Exception e)
        {e.printStackTrace();}
    }

}
