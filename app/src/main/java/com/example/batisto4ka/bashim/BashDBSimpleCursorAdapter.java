package com.example.batisto4ka.bashim;

import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by batisto4ka on 27.10.2014.
 * @author Natali Makarenko
 */
public class BashDBSimpleCursorAdapter extends SimpleCursorAdapter implements View.OnClickListener {
    /**
     * Log for cursor adapter error
     */
    final String    LOG_CURSOR_ADAPTER_ERROR="ADAPTER_FAILS IN POSITION";
    /**
     * ImageView for comics in listView with quotes
     */
    ImageView       comics;
    /**
     * Buttons "+"(rulez) and "-" (sux) in listView with quotes
     */
    Button          rulez,sux;
    /**
     * TextView with rating in listView with quotes
     */
    TextView        rating;
    /**
     *Cursor reference
     */
    Cursor          cursor;
    /**
     * DB column names
     */
    static String[] from={DB.TABLE1_ID_COLUMN2,DB.TABLE1_RATING_COLUMN3,DB.TABLE1_TEXT_COLUMN4,
                   DB.TABLE1_COMICS_COLUMN5};
    /**
     * Custom listView view's ids
     */
    static int[] to={R.id.id,R.id.rating,R.id.text,R.id.comics};

    /**
     * Constructor for adapting data from cursor,
     * @param cursor - cursor with query result from database
     */
    public BashDBSimpleCursorAdapter(Cursor cursor) {
        super(Main.activity,R.layout.listrowlayout, cursor,from,  to,0);
        this.cursor=cursor;
    }
     /**
     *Fill each view of adapter with data from quote
     * @param position - in current item in listview
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = super.getView(position,convertView,parent);
        try {
                comics = (ImageView) rowView.findViewById(R.id.comics);
                rating = (TextView) rowView.findViewById(R.id.rating);
                rulez = (Button) rowView.findViewById(R.id.btnPlus);
                sux = (Button) rowView.findViewById(R.id.btnMinus);
                comics.setOnClickListener(this);
                rulez.setOnClickListener(this);
                sux.setOnClickListener(this);
                comics.setTag(position);
                rulez.setTag(position);
                sux.setTag(position);
                if(cursor.moveToPosition(position)){
                if (cursor.getString(cursor.getColumnIndex(DB.TABLE1_COMICS_COLUMN5)) != null)
                    comics.setImageResource(R.drawable.yaoming);
                else
                    comics.setVisibility(View.GONE);
                if ((cursor.getString(cursor.getColumnIndex(DB.TABLE1_ID_COLUMN2)) == null) || (cursor.getString(cursor.getColumnIndex(DB.TABLE1_ID_COLUMN2)) == "")
                   || (cursor.getString(cursor.getColumnIndex(DB.TABLE1_RATING_COLUMN3)) == null)) {
                    rulez.setVisibility(View.GONE);
                    sux.setVisibility(View.GONE);
                    rating.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
                Log.e(LOG_CURSOR_ADAPTER_ERROR, String.valueOf(position));}
        return rowView;
    }
   /**
     * OnClick method for comics,rulez or sux click
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (!Main.activity.mainFragment.loaderState) {
            int id = view.getId();
            switch (id) {
                case R.id.comics: {
                    int i = (Integer) view.getTag();
                    if (cursor.moveToPosition(i)) {
                        if (cursor.getString(cursor.getColumnIndex(DB.TABLE1_COMICS_COLUMN5)) != null) {
                            Main.activity.mainFragment.pN.savePage();
                            Main.activity.mainFragment.onDataLoaded(Main.activity.mainFragment.IMAGE_LOAD, cursor.getString(cursor.getColumnIndex(DB.TABLE1_COMICS_COLUMN5)));
                        }
                    }
                }
                break;
                default: {
                    int i = (Integer) view.getTag();
                    if (cursor.moveToPosition(i)) {
                    if (id == R.id.btnPlus) {
                        Main.activity.mainFragment.onPost(Main.activity.mainFragment.RULEZ, cursor.getString(cursor.getColumnIndex(DB.TABLE1_ID_COLUMN2)), i);
                    } else if (id == R.id.btnMinus) {
                        Main.activity.mainFragment.onPost(Main.activity.mainFragment.SUX, cursor.getString(cursor.getColumnIndex(DB.TABLE1_ID_COLUMN2)), i);
                    }
                    }
                }
            }
        }
    }
}
