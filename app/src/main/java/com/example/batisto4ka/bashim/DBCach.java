package com.example.batisto4ka.bashim;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by batisto4ka on 21.10.2014.
 * Class for caching loaded data to database
 * @author Natali Makarenko
 */
public class DBCach {
    /**
     * Log for database caching
     */
    private static final String DBCACH_LOG="DB_CACH_LOG";
    /**
     * DB cach log message
     */
    private static final String DBCACH_ERROR="Error while caching data to db";
    /**
     * Minimum page index for cach
     */
    public static final byte    MIN_INDEX=1;
    /**
     * Maximum page index for cach (cach size, amount of page)
     */
    public static final byte    MAX_INDEX=10;
    /**
     *Database for caching quotes
     */
    public DB      db;
    Context ctx;

    public DBCach(Context ctx){
        db=new DB(ctx);
        this.ctx=ctx;
    }

    /**
     * Check db data, if page with pageNumber and type exists in cach updates its index on maximum
     * (last seen page) and other indexes, and page content. If page does not exists in db, method deletes data with minimum page index,
     * replaces it with loaded data and updates other page indexes
     * @param quote - cQuote object with data of loaded page
     * @param type - category page type @see PageNavigator.class
     *
     */
    public void putInCach(Quote quote,byte type){
        int pageNumber=0;
        Cursor cursor;
        try{
            if ((type==PageNavigator.NEW_PAGE)||(type==PageNavigator.RATING_PAGE))
                pageNumber=Integer.valueOf(quote.getCurPageNum());
            if (db.MainTable.CheckData(type,pageNumber))//if page exists in cach update its data and set page index as max
            {
                db.MainTable.updatePageIndexIfExist(type, pageNumber);
                db.MainTable.Delete(type,pageNumber);
                db.MainTable.addRecords(quote);
            }
            else//delete data with minimum index, write data with max, update indexes
            {
                db.MainTable.Delete(MIN_INDEX);
                db.MainTable.updatePageIndex();
                db.MainTable.addRecords(quote);

            }
        }catch (Exception e){Log.e(DBCACH_LOG, DBCACH_ERROR);}
    }
}
