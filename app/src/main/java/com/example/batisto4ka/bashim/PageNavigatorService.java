package com.example.batisto4ka.bashim;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Class of service for network operations
 * @author Natali Makarenko
 *
 * */
public class PageNavigatorService extends Service {
    /**
     * Url to connect
     */
    String   Url;
    /**
     * Tag that indicates operation for load pages with quotes
     */
    public static final byte    QUOTE_TAG=0;
    /**
     * Tag that indicates no data was loaded
     */
    public static final byte    NODATA_TAG=1;
    /**
     * Tag that indicates comics image loaded
     */
    public static final byte    IMAGE_TAG=2;
    /**
     * tag that indicates change quote ration operation
     */
    public static final byte    RATING_TAG=3;
    /**
     * Value of url to connect for intent
     */
    public static final String  INTENT_URL="URL";
    /**
     * Value of operation type tag for intent
     */
    public static final String  INTENT_TAG="TAG";
    /**
     * Value of page category type for intent
     */
    public static final String  INTENT_PAGE_TAG_TYPE="PAGE_TAG_TYPE";
    /**
     * Value of referrer of POST method for intent
     */
    public static final String  INTENT_REFFERER="refferer";
    /**
     * Value of url for change rating of POST method for intent
     */
    public static final String  INTENT_RATING_URL="ratingUrl";
    /**
     * Id of quote for POST method for intent
     */
    public static final String  INTENT_ID="id";
    /**
     * Value "/rulez" or "/sux" operation for POST method for intent
     */
    public static final String  INTENT_TAGRZ="tagrz";
    /**
     * Position of quote in listView for intent
     */
    public static final String  INTENT_POSITION="position";
    /**
     * Rate value 1 (if rulez), -1 - if sux for intent
     */
    public static final String  INTENT_RATE="rate";
    /**
     * Current page String value for intent
     */
    public static final String  INTENT_CURRENT_PAGE="CurrentPage";
    /**
     * Image source url for intent
     */
    public static final String  INTENT_IMAGE_SRC="IMAGE_SRC";
    /**
     * Quote for intent
     */
    public static final String  INTENT_QUOTE="QUOTE";
    /**
     * Operation tag
     */
    byte            tag;
    /**
     *  Page category type
     */
    byte pageTag;
    /**
     * HtmlParseHelper object to parse data
     */
    HtmlParseHelper pH;
    /**
     * Network(connection) state
     */
    boolean NETWORK;
    /**
     * Current page string value to connect
     */
    private String  CurrentPage;
    /**
     * Parsed in Quote object page
     */
    private Quote Quotes;
    /**
     * Comics image source url
     */
    private String  imgSrc;
    /**
     * Object for caching pages
     */
    DBCach  dbCach;

    public PageNavigatorService() {
    }
    //start service
    public int onStartCommand(Intent intent, int flags, int startId) {
        pH=new HtmlParseHelper();
        NETWORK=false;
        this.tag=intent.getByteExtra(INTENT_TAG,(byte)4);
        dbCach=new DBCach(this);
        if (this.tag==RATING_TAG)
        {
            try {
                intent.getStringExtra(INTENT_REFFERER);
                intent.getStringExtra(INTENT_RATING_URL);
                intent.getStringExtra(INTENT_ID);
                intent.getStringExtra(INTENT_TAGRZ);
                intent.getIntExtra(INTENT_POSITION,0);
                intent.getByteExtra(INTENT_RATE,(byte)0);
            new PostThread(intent.getStringExtra(INTENT_REFFERER),intent.getStringExtra(INTENT_RATING_URL),intent.getStringExtra(INTENT_ID),
                    intent.getStringExtra(INTENT_TAGRZ),intent.getIntExtra(INTENT_POSITION,0),intent.getByteExtra(INTENT_RATE,(byte)0)).execute();
            }catch(Exception e){NETWORK=false;}
        }
        else {
            this.Url = intent.getStringExtra(INTENT_URL);
            pageTag=intent.getByteExtra(INTENT_PAGE_TAG_TYPE,(byte)0);
            new myTask().execute();//run();
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Main method that implements connection to Url and gets certain data
     */
    void run()
    {
        Document CurrentDocument;
        int i=0;
        try {
            CurrentDocument = pH.connect(Url);

            if (CurrentDocument != null) {
                CurrentPage=Url;

                switch (tag) {
                    case QUOTE_TAG:
                        Quotes = pH.getData(CurrentDocument, pH.QUOTE);
                        break;

                    case IMAGE_TAG:
                        imgSrc=(pH.getImage(CurrentDocument, pH.IMAGE));
                        break;
                    default:
                        break;
                }
                NETWORK=true;
            }
            else NETWORK=false;
        }catch (Exception e){
            NETWORK=false;
        }
        stop();
    }

    /**
     * Method, that send parsed data in broadcast receiver
     */
    void stop (){
        Intent intent = new Intent(PageNavigator.BROADCAST_ACTION);
        if (NETWORK) {

            switch (tag) {
                case QUOTE_TAG:
                    Quotes.setPageIndex(DBCach.MAX_INDEX);
                    Quotes.setPageType(pageTag);
                    intent.putExtra(INTENT_QUOTE,Quotes.toBundle());
                    intent.putExtra(INTENT_CURRENT_PAGE,CurrentPage);
                    intent.putExtra(INTENT_TAG,QUOTE_TAG);
                    dbCach.putInCach(Quotes,pageTag);
                    sendBroadcast(intent);
                    //fragment.onDataLoaded(Quotes);
                    break;
                case IMAGE_TAG:
                    intent.putExtra(INTENT_IMAGE_SRC,imgSrc);
                    intent.putExtra(INTENT_CURRENT_PAGE,CurrentPage);
                    intent.putExtra(INTENT_TAG,IMAGE_TAG);
                    sendBroadcast(intent);//fragment.onDataLoaded(fragment.IMAGE_SHOW, GetImgSrc());
                    break;
                default:// NODATA_TAG:
                    intent.putExtra(INTENT_TAG,NODATA_TAG);
                    sendBroadcast(intent);
                    break;
            }
        }
        else{
            intent.putExtra(INTENT_TAG,NODATA_TAG);
            sendBroadcast(intent);

        }
        stopSelf();
    }

    /**
     * Class that implements page parsing operations in background
     */
 class  myTask extends AsyncTask<String, Void, String>{

     @Override
     protected String doInBackground(String... strings) {
         run();
         return null;
     }
     @Override
     protected void onPostExecute(String result) {

     }
    }

    /**
     * Class that implements POST method for changing rating of quote in background
     */
    class PostThread extends AsyncTask<String, Void, String> {
        String referrer;
        String ratingUrl;
        String id;
        String tag;
        int    position;
        byte   rate;
        boolean state;

        public PostThread (String src, String url,String id,String tag,int position, byte rate){
            this.referrer=src;       //http://bash.im/index &&&&&&
            this.ratingUrl=url;///quote/4/rulez
            this.id=id;
            this.tag=tag;
            this.position=position;
            this.rate=rate;
            state=false;
        }
        /**
         * Method that sends post query with rulez or sux quote
         * @param arg
         * @return
         */
        @Override
        protected String doInBackground(String... arg) {

            try {
                Document doc= Jsoup.connect(PageNavigator.BASE_PAGE/*"http://bash.im"*/ + ratingUrl).data("act",/*"rulez"*/tag).data("quote",id).followRedirects(false).referrer(referrer).userAgent("Opera/9.80 (Windows NT 6.2; WOW64) Presto/2.12.388 Version/12.14").method(Connection.Method.POST).post();
                if (doc!=null)
                    state=true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Send  broadcast receiver with result: if connection was successful, sends data back in intent with RATING_TAG?
         * if connection failed - sends intent with tag NODATA_TAG
         * @param result -
         */
        @Override
        protected void onPostExecute(String result) {
            Intent intent = new Intent(PageNavigator.BROADCAST_ACTION);
            if (state){
                intent.putExtra(INTENT_RATE,this.rate);
                intent.putExtra(INTENT_POSITION,this.position);
                intent.putExtra(INTENT_TAG,RATING_TAG);
                sendBroadcast(intent);//fragment.onDataLoaded(fragment.IMAGE_SHOW, GetImgSrc());
            }
            else{
                intent.putExtra(INTENT_TAG,NODATA_TAG);
                sendBroadcast(intent);
            }
            stopSelf();
        }
    }

}
