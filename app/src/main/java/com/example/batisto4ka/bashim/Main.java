package com.example.batisto4ka.bashim;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

/**
 * Main activity class
 * @author Natali Makarenko
 */

public class Main extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * SaveInstanceState Bundle string value for mTitle
     */
    public static final String BUNDLE_MTITLE="mTitle";
    /**
     * Bundle string value for mainFragment
     */
    public static final String BUNDLE_FRAGMENT="mainFragment";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    public NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Fragment managing the behaviors, interactions and presentation of the BASH.IM content.
     */
    public PlaceHolderFragment mainFragment;

    /**
     * Used to store the last screen title.
     */
   public static CharSequence mTitle;
    /**
    * Main activity static variable
    */
    public static Main activity;
    /**
     * Activity on Create method
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        activity=this;
        if(savedInstanceState!=null)
           mainFragment = (PlaceHolderFragment)getSupportFragmentManager().getFragment(savedInstanceState, "mainFragment");
        else
            {
                 mainFragment =new PlaceHolderFragment();
                 android.support.v4.app.FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
                 fTrans.add(R.id.container, mainFragment);
                 fTrans.commit();
            }
        mNavigationDrawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        // Set up the drawer
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }
      /**
     * Navigation Drawer list view item selection method
     * @param position - selected position
     */
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content
        if (mainFragment!=null) {
            String maxNumber=mainFragment.getMaxPageNumber(position);
            mainFragment.pN.setMaxPageNumber(Integer.valueOf(maxNumber));
            mainFragment.pN.setCurrentPageNumber(Integer.valueOf(maxNumber));
            switch (position) {
                case PageNavigator.NEW_PAGE:
                    mainFragment.ShowPage(PageNavigator.NEW_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.NEW_PAGE,maxNumber);
                break;
                case PageNavigator.RANDOM_PAGE:
                    mainFragment.ShowPage(PageNavigator.RANDOM_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.RANDOM_PAGE,maxNumber);
                break;
                case PageNavigator.BEST_PAGE:
                    mainFragment.ShowPage(PageNavigator.BEST_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.BEST_PAGE,maxNumber);
                break;
                case PageNavigator.RATING_PAGE:
                    mainFragment.ShowPage(PageNavigator.RATING_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.RATING_PAGE,maxNumber);
                break;
                case PageNavigator.ABYSS_PAGE:
                    mainFragment.ShowPage(PageNavigator.ABYSS_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.ABYSS_PAGE,maxNumber);
                break;
                case PageNavigator.ABYSS_TOP_PAGE:
                    mainFragment.ShowPage(PageNavigator.ABYSSTOP_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.ABYSS_TOP_PAGE,maxNumber);
                break;
                case PageNavigator.ABYSS_BEST_PAGE:
                    mainFragment.ShowPage(PageNavigator.ABBYSBEST_SPAGE, PageNavigator.CATEGORY_TYPE,PageNavigator.ABYSS_BEST_PAGE,maxNumber);
                break;
                default:
                    break;
            }
        }

    }

    /**
     * Method that restores Action Bar after closing
     */
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
    }

    /**
     * Activity onCreateOptionsMenu method
     * @param menu
     * @return
     */
     @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
           getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Activity onPrepareOptionsMenu method
     * sets title of current loaded page in actiob bar
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        menu.getItem(0).setTitle(mTitle);
    return true;
    }

    /**
     * Action bar update button callback
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.update) {
            if ((mainFragment!=null)&&(!mainFragment.loaderState))
                mainFragment.ShowPage(mainFragment.pN.getPage(), mainFragment.pN.UPDATE_TYPE,mainFragment.pN.getPageType(),String.valueOf(mainFragment.CurrentPage.getMaxPageNum()));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method that restores instanceState of activity
     * @param savedInstanceState
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTitle=savedInstanceState.getString(BUNDLE_MTITLE);
    }
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            getSupportFragmentManager().putFragment(outState, BUNDLE_FRAGMENT, mainFragment);
            outState.putString(BUNDLE_MTITLE, mTitle.toString());
        }catch (Exception e){}
    }
}
