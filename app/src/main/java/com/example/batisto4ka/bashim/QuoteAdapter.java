package com.example.batisto4ka.bashim;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by batisto4ka on 01.10.2014.
 * Adapter for listView that contains data in Quote object
 * @author Natali Makarenko
 */
public class QuoteAdapter extends BaseAdapter implements View.OnClickListener {

        private Quote quote;
        LayoutInflater inflater;

        /**
         * Constructor for adapting data from cQuote object
         * @param q - Quote object with data to adapt into listView
         */
        public QuoteAdapter(Quote q) {
            super();
            this.quote=q;
            inflater = (LayoutInflater) Main.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

    /**
     * Method, that returns listView item count
     * @return - quote item count
     */
        @Override
        public int getCount() {
            if (quote!=null)
                return quote.size();
            else return 0;
        }

    /**
     * Gets item from list
     * @param i - position
     * @return - quote object
     */
        @Override
        public Object getItem(int i) {

            return quote;
        }

    /**
     * Gets selected item id
     * @param i - position
     * @return - item id from database
     */
        @Override
        public long getItemId(int i) {
            return i;
        }

        /**
         *Gets view from adapter
         * @param position - in current item in listview
         * @param convertView
         * @param parent
         * @return
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           /* LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
            View rowView = inflater.inflate(R.layout.listrowlayout, parent, false);
            TextView id = (TextView) rowView.findViewById(R.id.id);
            ImageView comics = (ImageView) rowView.findViewById(R.id.comics);
            comics.setTag(position);
            comics.setOnClickListener(this);
            TextView mtext = (TextView) rowView.findViewById(R.id.text);
            TextView rating=(TextView)rowView.findViewById(R.id.rating);
            Button rulez=(Button)rowView.findViewById(R.id.btnPlus);
            rulez.setTag(position);
            rulez.setOnClickListener(this);
            Button sux=(Button)rowView.findViewById(R.id.btnMinus);
            sux.setTag(position);
            sux.setOnClickListener(this);

            id.setText(quote.getId()[position]);
            mtext.setText(quote.getText()[position]);
            if(quote.getComicsRef()[position]!=null)
                comics.setImageResource(R.drawable.yaoming);
            else
                comics.setVisibility(View.GONE);
            if (quote.getRating()[position]!=null)
                rating.setText(quote.getRating()[position]);
            if((quote.getId()[position]==null)||(quote.getId()[position]=="")||(quote.getRating()[position]==null))
            {
                rulez.setVisibility(View.GONE);
                sux.setVisibility(View.GONE);
                rating.setVisibility(View.GONE);
            }
            return rowView;
        }

        /**
         * OnClick method for click on comics imageView,rulez or sux buttons
         * @param view - clicked view
         */
        @Override
        public void onClick(View view) {
            int id=view.getId();
            switch (id)
            {
                case R.id.comics:{ int i=(Integer)view.getTag();
                    if (quote.getComicsRef()[i]!=null) {
                        Main.activity.mainFragment.pN.savePage();
                        Main.activity.mainFragment.onDataLoaded(Main.activity.mainFragment.IMAGE_LOAD, quote.getComicsRef()[i]);
                    }
                }break;
                default:{
                    int i=(Integer)view.getTag();
                    if (id==R.id.btnPlus){
                        Main.activity.mainFragment.onPost(Main.activity.mainFragment.RULEZ,quote.getId()[i],i);
                    }
                    else if (id==R.id.btnMinus){
                        Main.activity.mainFragment.onPost(Main.activity.mainFragment.SUX,quote.getId()[i],i);
                    }
                }
            }
        }
}

