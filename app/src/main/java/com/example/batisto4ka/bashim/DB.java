package com.example.batisto4ka.bashim;

/**
 * Created by Bender on 03.10.2014.
 * Class for working with database
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DB {

    /**
     * Log for DB
     */
    final String LOG_DB="DB_LOG";
    /**
     * Delete result
     */
    final String LOG_DB_DELETE="Deleted from db";
    /**
     * Database version
     */
    public static final int DATABASE_VERSION=1;
    /**
     * Database name
     */
    public static final String DATABASE_NAME="BashIm.db";
    /**
     * Table name
     */
    public static final String TABLE1_NAME="Main";
    /**
     * id field in table
     */
    public static final String TABLE1_id_COLUMN1="_id";
    /**
     * quote id field in table
     */
    public static final String TABLE1_ID_COLUMN2="ID";
    /**
     * quote rating field in table
     */
    public static final String TABLE1_RATING_COLUMN3="RATING";
    /**
     * quote text field in table
     */
    public static final String TABLE1_TEXT_COLUMN4="TEXT";
    /**
     * quote comics link field in table
     */
    public static final String TABLE1_COMICS_COLUMN5="COMICS";
    /**
     * quote page number field in table
     */
    public static final String TABLE1_PAGE_NUMBER_COLUMN6="PAGE_NUMBER";
    /**
     * quote page category type field in table
     */
    public static final String TABLE1_PAGE_TYPE_COLUMN7="PAGE_TYPE";
    /**
     * page index for cach field in table
     */
    public static final String TABLE1_PAGE_INDEX_COLUMN8="PAGE_INDEX";

    /**
     * Column numbers of db columns
     */
    public static final byte TABLE1_id_COLUMN1_NUMBER=0;
    /**
     * quote id field in table
     */
    public static final byte TABLE1_ID_COLUMN2_NUMBER=1;
    /**
     * quote rating field in table
     */
    public static final byte TABLE1_RATING_COLUMN3_NUMBER=2;
    /**
     * quote text field in table
     */
    public static final byte TABLE1_TEXT_COLUMN4_NUMBER=3;
    /**
     * quote comics link field in table
     */
    public static final byte TABLE1_COMICS_COLUMN5_NUMBER=4;
    /**
     * quote page number field in table
     */
    public static final byte TABLE1_PAGE_NUMBER_COLUMN6_NUMBER=5;
    /**
     * quote page category type field in table
     */
    public static final byte TABLE1_PAGE_TYPE_COLUMN7_NUMBER=6;
    /**
     * page index for cach field in table
     */
    public static final byte TABLE1_PAGE_INDEX_COLUMN8_NUMBER=7
            ;
    public ContentValues 	cv = new ContentValues();

    private Context	        mCtx;
    private DBHelper 		BashDBHelper;
    public  SQLiteDatabase 	BashDB;
    private Cursor			cursor;
    /**
     * Database table object
     */
    public  DBTableClass 	MainTable;

    /**
     * db class constructor
     * @param ctx
     */
    public DB(Context ctx) {
        mCtx = ctx;
        MainTable=new DBTableClass();
        BashDBHelper = new DBHelper(mCtx, DATABASE_NAME, null, DATABASE_VERSION);
        BashDB = BashDBHelper.getWritableDatabase();
    }

    /**
     * db class for table
     */
    public class DBTableClass {
        /**
         * quote record to database
         */
        Quote Record;
        /**
         * field name of column 1 for _id
         */
        public String column_idName;
        /**
         * field name of column 2 for quote id
         */
        public String columnIdName;
        /**
         * field name of column 3 for quote rating
         */
        public String columnRatingName;
        /**
         * field name of column 4 fro quote content
         */
        public String columnTextName;
        /**
         * field name of column 5 for quote comics
         */
        public String columnComicsName;
        /**
         * field name of column 6 for quote page number
         */
        public String columnPageNumberName;
        /**
         * field name of column 7 for page category type
         */
        public String columnPageTypeName;
        /**
         * field name of column 8 for page index for caching
         */
        public String columnPageIndexName;
        /**
         * table name
         */
        public String tableName;


        public DBTableClass() {
            column_idName = TABLE1_id_COLUMN1;
            columnIdName = TABLE1_ID_COLUMN2;
            columnRatingName = TABLE1_RATING_COLUMN3;
            columnTextName = TABLE1_TEXT_COLUMN4;
            columnComicsName = TABLE1_COMICS_COLUMN5;
            columnPageNumberName = TABLE1_PAGE_NUMBER_COLUMN6;
            columnPageTypeName = TABLE1_PAGE_TYPE_COLUMN7;
            columnPageIndexName = TABLE1_PAGE_INDEX_COLUMN8;
            tableName = TABLE1_NAME;
            Record = new Quote(1);
        }

        protected String getId() {
            return Record.getId()[0];
        }

        protected void setId(String id) {

            Record.setId(0,id);
            cv.put(columnIdName, id);
        }

        /**
         * add record to db
         */

        public void addRecords(Quote Record) {
            cv.clear();
            for (int i = 0; i < Record.size; i++) {
                BashDB.beginTransaction();
                try {
                    cv.put(columnIdName, Record.getId()[i]);
                    cv.put(columnRatingName, Record.getRating()[i]);
                    cv.put(columnTextName, Record.getText()[i]);
                    cv.put(columnComicsName, Record.getComicsRef()[i]);
                    cv.put(columnPageNumberName, Record.getCurPageNum());
                    cv.put(columnPageTypeName, Record.getPageType());
                    cv.put(columnPageIndexName, Record.getPageIndex());
                    long result = BashDB.insert(tableName, null, cv);
                    Log.e(LOG_DB,String.valueOf(result));
                    BashDB.setTransactionSuccessful();
                } finally {
                    BashDB.endTransaction();
                }
            }
        }

       /**
         * Delete all data from table
        */
       public void Delete()
       {
           BashDB.delete(tableName,null,null);
       }
        /**
         * Delete  page from database by category type and page number
         * @param type - category type of page to delete @see PageNavigator.class
         * @param pageNumber - number of page to delete
         */
       public void Delete(byte type, int pageNumber)
        {
           String whereClause=columnPageTypeName+"="+type+" AND "+columnPageNumberName+"="+pageNumber;
            int result=BashDB.delete(tableName,whereClause,null);
            Log.i(LOG_DB_DELETE,String.valueOf(result));
        }

        /**
         * Delete page from database by page category type and cach index
         * @param index - page index
         *
         */
        public void Delete(byte index)
        {
            String whereClause=columnPageIndexName+"="+index;
            int result=BashDB.delete(tableName,whereClause,null);
            Log.i(LOG_DB_DELETE,String.valueOf(result));
        }
       /**
         * Check if data page with number and category type exists in db table
         * @param type - page category type @see: PageNavigator.class
         * @param pageNumber - page number
         * @return - true- if exists, false -if not exists
         */
        public boolean CheckData(byte type,int pageNumber){
            String sql;
            if (pageNumber!=0)
                sql= "select "+columnPageIndexName+" from " + tableName+" WHERE "+columnPageTypeName+"="+type+" AND "+columnPageNumberName+"="+pageNumber;
            else
            sql="select "+columnPageIndexName+" from " + tableName+" WHERE "+columnPageTypeName+"="+type;
            cursor = BashDB.rawQuery(sql, null);
            if (cursor.moveToFirst())
                return true;
            else return false;
        }

        /**
         * Check if data with page category type in table has index
         * @param index - cach index to check
         * @param type - page type to check @see PageNavigator.class
         * @return - true-if has, false-if hasn't
         */
       public boolean CheckDataByIndex(byte index,byte type){
            String sql = "select * from " + tableName+" WHERE "+columnPageTypeName+"="+type+" AND "+columnPageIndexName+"="+index;
            cursor = BashDB.rawQuery(sql, null);
            if (cursor.moveToFirst())
                return true;
            else return false;
        }

        /**
         * Check  by category type and page number if index of page exist
         * @param type - page category type @see PageNavigator.class
         * @param pageNumber - page number
         * @param pageIndex - page index to check
         * @return - true if exists, false if not exists
         */
        public boolean CheckIndexByTypeAndPageNumber(byte type, String pageNumber,byte pageIndex){
            byte currentPageIndex=0;
            String sql = "select "+columnPageIndexName+" from " + tableName+" WHERE "+columnPageTypeName+"="+type+" AND "+columnPageNumberName+"="+pageNumber;
            cursor = BashDB.rawQuery(sql, null);
            if (cursor.moveToFirst())
            {
                do{
                    currentPageIndex=(byte)cursor.getInt(cursor.getColumnIndex(columnPageIndexName));
                }while(cursor.moveToNext());
            }
           return  (currentPageIndex==pageIndex);
        }
         /**
         * Update page indexes in db of page with category type and page number
         */
        public void updatePageIndex(){
          String sql="UPDATE " + tableName+" SET "+columnPageIndexName+"="+columnPageIndexName+"-1";
          BashDB.execSQL(sql);
        }
        /**
         * Update page indexes in db of page with category type and page number
         * @param type - page category type @see PageNavigator.class
         * @param currentPageNumber - page number
         */
        public void updatePageIndexIfExist(byte type,int currentPageNumber){
           String sql = "UPDATE " + tableName+" SET "+columnPageIndexName+"="+columnPageIndexName+"-1 WHERE "+
                    columnPageIndexName+">(SELECT "+columnPageIndexName+" FROM "+tableName+" WHERE "+columnPageNumberName+
                    "="+currentPageNumber+" AND "+columnPageTypeName+"="+type+")";
            cursor = BashDB.rawQuery(sql, null);
        }
        /**
         * Return table data with page by type and page number
         * @param type - category page type @see PageNavigator.class
         * @param pageNumber - page number
         * @return - data by page and type
         */
        public Cursor ShowTable(byte type, int pageNumber) {
            String sql;
            if (pageNumber!=0)
                sql= "select * from " + tableName+" WHERE "+columnPageTypeName+"="+type+
                        " AND "+ columnPageNumberName+"="+pageNumber;
            else
            sql="select * from " + tableName+" WHERE "+columnPageTypeName+"="+type;
            cursor=BashDB.rawQuery(sql, null);
           return cursor;
        }
        public Cursor ShowTable(byte index, byte type) {
            String sql;
            sql= "select * from " + tableName+" WHERE "+columnPageTypeName+"="+type+
                        " AND "+ columnPageIndexName+"="+index;
            cursor=BashDB.rawQuery(sql, null);
            return cursor;
        }
    }

    /**
     * Close database
     */
    public void close() {
        if (BashDBHelper!=null) BashDBHelper.close();
    }

    /**
     *  db helper
     *  class helper for database create and control
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }
        /**
         *  Create and fill table of database
         *  @param db - SQLiteDatabase
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            String sql="CREATE TABLE " + MainTable.tableName + " (" +
                    MainTable.column_idName + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MainTable.columnIdName + " TEXT,"+
                    MainTable.columnRatingName+ " TEXT,"+
                    MainTable.columnTextName +" TEXT ,"+
                    MainTable.columnComicsName+" TEXT,"+
                    MainTable.columnPageNumberName+" INTEGER,"+
                    MainTable.columnPageTypeName+" SMALLINT,"+
                    MainTable.columnPageIndexName+" SMALLINT)";
            db.execSQL(sql);
        }

        /**
         * Update database
         * @param db - database to update
         * @param oldVersion - old version number
         * @param newVersion - new version number
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+ MainTable.tableName);
            onCreate(db);
        }
    }
}

