package com.example.batisto4ka.bashim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Activity class for open comics image
 * @author Natali Makarenko
 */
public class ImageActivity extends ActionBarActivity {
    /**
     * Image view of where comics shows
     */
    ImageView imgView;
    /**
     * Progress bar for displaying process of loading image
     */
    ProgressBar progressBar;
    /**
     * Activity on Create method
     * @param - saved instance state of activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_image);
        imgView = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();
        String src = intent.getStringExtra(PlaceHolderFragment.INTENT_IMAGE_SRC);
        imgView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        progressBar=(ProgressBar) findViewById(R.id.imageProgressBar);
        FrameLayout view=(FrameLayout)findViewById(R.id.imageContainer);
        view.setOnTouchListener(new Zoom(view, imgView, Zoom.Anchor.CENTER));
        Picasso.with(this).load(src).into(imgView, new Callback.EmptyCallback() {
            @Override public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onError() {
                progressBar.setVisibility(View.GONE);
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }


}
