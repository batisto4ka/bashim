package com.example.batisto4ka.bashim;

import android.os.Bundle;

/**
 * Created by batisto4ka on 01.10.2014.
 * Class which represents quotes
 * @author Natali Makarenko
 */
public class Quote {
    /**
     * String values for put quote object in bundle object
     */
    public static final String BUNDLE_QUOTE_ID="quote_id";
    public static final String BUNDLE_QUOTE_RATING="quote_rating";
    public static final String BUNDLE_QUOTE_TEXT="quote_text";
    public static final String BUNDLE_QUOTE_COMICS="quote_comics";
    public static final String BUNDLE_MAX_PAGE_NUMBER="max_page_num";
    public static final String BUNDLE_CUR_PAGE_NUMBER="cur_page_num";
    public static final String BUNDLE_PAGE_TYPE="page_type";
    public static final String BUNDLE_PAGE_INDEX="page_index";
    public static final String BUNDLE_SIZE="size";

    private String[] Id;
    private String[] Text;
    private String[] Rating;
    private String[] comicsReference;
    /**
     * loaded page number
     */
    private String  currentPageNumber;
    /**
     * max page number for page category
     */
    private int     maxPageNumber;
    /**
     * number of page category type
     */
    private byte    pageType;
    /**
     * page index for cach
     */
    private byte    pageIndex;
    /**
     * quote array size
     */
    public int     size;
    /**
     * constructor
     * @param size - count of quotes
     */
    public Quote(int size)
    {
        this.Id=new String[size];
        this.Text=new String[size];
        this.comicsReference=new String[size];
        this.Rating=new String[size];
        this.size=size;
    }

    /**
     * Create cQuote with content from Bundle
     * @param b - bundle object
     */
    public Quote(Bundle b){

        this.Id=b.getStringArray(BUNDLE_QUOTE_ID);
        this.Rating=b.getStringArray(BUNDLE_QUOTE_RATING);
        this.Text=b.getStringArray(BUNDLE_QUOTE_TEXT);
        this.comicsReference=b.getStringArray(BUNDLE_QUOTE_COMICS);
        this.maxPageNumber=b.getInt(BUNDLE_MAX_PAGE_NUMBER,0);
        this.currentPageNumber=b.getString(BUNDLE_CUR_PAGE_NUMBER);
        this.pageType=b.getByte(BUNDLE_PAGE_TYPE);
        this.pageIndex=b.getByte(BUNDLE_PAGE_INDEX);
        this.size=b.getInt(BUNDLE_SIZE,Text.length);
    }
    /**
     * Set comics link
     * @param page
     */
    public void setComicsRef(int i,String page)
    {
        comicsReference[i]=page;
    }

    /**
     * Get comics link by id
     *@return
     */
    String[] getComicsRef(){return comicsReference;}

    /**
     * set quote rating
     *@param r
     */
    public void setRating(int i,String r){Rating[i]=r;}

    /**
     * get quote rating
     * @return
     */
    public String[] getRating(){return Rating;}

    /**
     * Set current page number
     * @param page
     */
     public void setCurPageNum(String  page)
    {
        currentPageNumber=page;
    }

    /**
     * Get current page number
     * @return
     */
    public String getCurPageNum()
    {
        return currentPageNumber;
    }

    /**
     * Set maximum page number
     * @param num
     */
    public void setMaxPageNum(int num)
    {
        this.maxPageNumber=num;
    }

    /**
     * get maximum page number
     * @return
     */
    public int  getMaxPageNum()
    {
       return this.maxPageNumber;
    }

    public void setPageType(byte type){
        pageType=type;
    }
    public byte getPageType(){
        return pageType;
    }
    public void setPageIndex(byte index){
        pageIndex=index;
    }
    public byte getPageIndex(){
        return pageIndex;
    }
    public void setId(int i,String id)
    {
        this.Id[i]=id;
    }
    public void setText (int i,String ref)
    {
        this.Text[i]=ref;
    }
    public String[] getId()
    {
        return Id;
    }
    public String[] getText()
    {
        return Text;
    }
    public int size(){return this.size;}

    /**
     * Put cQuote to Bundle object
     * @return - bundle with quotes
     */
    public Bundle toBundle(){
        Bundle b = new Bundle();
        b.putStringArray(BUNDLE_QUOTE_ID, getId());
        b.putStringArray(BUNDLE_QUOTE_RATING, getRating());
        b.putStringArray(BUNDLE_QUOTE_TEXT, getText());
        b.putStringArray(BUNDLE_QUOTE_COMICS, getComicsRef());
        b.putByte(BUNDLE_PAGE_TYPE,getPageType());
        b.putByte(BUNDLE_PAGE_INDEX, getPageIndex());
        b.putInt(BUNDLE_MAX_PAGE_NUMBER, getMaxPageNum());
        b.putString(BUNDLE_CUR_PAGE_NUMBER, getCurPageNum());
        b.putInt(BUNDLE_SIZE,size);
        return b;
    }

}
