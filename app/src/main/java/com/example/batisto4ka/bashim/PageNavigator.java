package com.example.batisto4ka.bashim;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Class for that implements navigation between pages of site www.bash.im
 * Created by batisto4ka on 02.10.2014.
 * @author natali Slinko
 *
 */
public class PageNavigator  {
    /**
     * Code of operation update current page
     */
    public static final byte    UPDATE_TYPE=1;
    /**
     * Code of operation load one of category page
     */
    public static final byte    CATEGORY_TYPE=2;
    /**
     * String value of base site page
    */
    public static final String  BASE_PAGE="http://bash.im";
    /**
     * String value of news page
     */
    public static final String  NEW_SPAGE="/index";
    /**
     * String value of random page
     */
    public static final String  RANDOM_SPAGE="/random";
    /**
     * String value of best page
     */
    public static final String  BEST_SPAGE="/best";
    /**
     * String value of by rating page
     */
    public static final String  RATING_SPAGE="/byrating";
    /**
     * String value of abbys page
     */
    public static final String  ABYSS_SPAGE="/abyss";
    /**
     * String value of top of the abyss page
     */
    public static final String  ABYSSTOP_SPAGE="/abysstop";
    /**
     * String value of best of the abyss page
     */
    public static final String  ABBYSBEST_SPAGE="/abyssbest";
    /**
     * Page category number for "Новые"
     */
    public static final byte NEW_PAGE=0;
    /**
     * Page category number for "Случайнфе"
     */
    public static final byte RANDOM_PAGE=1;
    /**
     * Page category number for "Лучшиее"
     */
    public static final byte BEST_PAGE=2;
    /**
     * Page category number for "По рейтингу"
     */
    public static final byte RATING_PAGE=3;
    /**
     * Page category number for "Бездна"
     */
    public static final byte ABYSS_PAGE=4;
    /**
     * Page category number for "Топ бездны"
     */
    public static final byte ABYSS_TOP_PAGE=5;
    /**
     * Page category number for "Лучшее бездны"
     */
    public static final byte ABYSS_BEST_PAGE=6;
    /**
     * Broadcast receiver action
     */
    public static final String BROADCAST_ACTION="com.example.batisto4ka.navigationdrawer";
    /**
     * SharedPreferences string value for saving current page string value
     */
    public static final String SHARED_PREFERENCES_PN_CURRENT_PAGE="pN_CurrentPage";
    /**
     * SharedPreferences string value for saving base page string value
     */
    public static final String SHARED_PREFERENCES_PN_BASE_PAGE="pN_BasePage";
    /**
     * SharedPreferences string value for saving current page number string value
     */
    public static final String  SHARED_PREFERENCES_PN_CURRENT_PAGE_NUMBER="pN_CurrentPageNumber";
    /**
     * SharedPreferences string value for saving max page number string value
     */
    public static final String SHARED_PREFERENCES_PN_MAX_PAGE_NUMBER="pN_MaxPageNumber";
    /**
     * SharedPreferences string value for saving page category type string value
     */
    public static final String SHARED_PREFERENCES_PN_PAGE_TYPE="pN_pageType";
    /**
     * SharedPreferences string value for saving max page number string value for page New
     */
    public static final String SHARED_PREFERENCES_PN_NEWS_PAGE_MAX_NUMBER="pN_newsMaxPageNumber";
    /**
     * Broadcast receiver for getting result from service which loads data
     */
    private  BroadcastReceiver       br;
    /**
     * String value of current page, base page and previous page
     */
    private String                  CurrentPage,BasePage,previousPage;

    private Quote Quotes;
    //private PlaceHolderFragment fragment;
    private String              imgSrc;
    //byte                        tag;
    /**
     * Page category type
     */
    byte                         pageTagType;
    /**
     * Maximum page number
     *
     */
    int maxPageNumber;
    /**
     * Current page number
     */
    int currentPageNumber;
    /**
     * Max number of page  News
     */
    String newsPageMaxNumber;

    public PageNavigator()
    {
       InitBroadcastReceiver();
    }

    /**
     * Method that saves String of current page to previous page
     */
    public void savePage(){
        previousPage=CurrentPage;
    }

    /**
     * Method that returns broadcast receiver
     * @return - broadcast receiver
     */
    public BroadcastReceiver getReceiver(){return br;}
    /**
     * Restores current page string value from previous page
     */
    public void restorePage(){
        CurrentPage=previousPage;
    }

    /**
     * Load category page
     * @param Url - url to connect with jsoup connect
     * @param base - base page string value
     * @param type - category page type
     */
     public void LoadCatPage(String Url,String base,byte type)
    {
       LoadPage(Url+base,type);
       setBasePage(base);
       pageTagType=type;
    }

    /**
     * Connect to url and load page data
     * @param Url - - url to connect with jsoup connect
     * @param type - - category page type
     */
   public void LoadPage(String Url,byte type)
    {
        pageTagType=type;
        Intent intent = new Intent(Main.activity, PageNavigatorService.class);
        intent.putExtra(PageNavigatorService.INTENT_URL, Url);
        intent.putExtra(PageNavigatorService.INTENT_TAG, PageNavigatorService.QUOTE_TAG);
        intent.putExtra(PageNavigatorService.INTENT_PAGE_TAG_TYPE,type);
        Main.activity.startService(intent);
    }

    /**
     * Load and show comics image
     * @param Url - url of comics image
     */
    public void LoadImage(String Url)
    {
        Intent intent = new Intent(Main.activity, PageNavigatorService.class).putExtra(PageNavigatorService.INTENT_URL, Url)
                .putExtra(PageNavigatorService.INTENT_TAG, PageNavigatorService.IMAGE_TAG);
        Main.activity.startService(intent);
    }

    /**
     * Post quote (like or dislike)
     * @param ratingUrl - url of quote rating
     * @param id - quote id
     * @param tagrz - tag with act "rulez" or "sux"
     * @param position - position of touched quote in listview
     * @param rate - rate value of act:1 - if like, -1-if dislike
     */
    public void LikeQuote(String ratingUrl, String id,String tagrz, int position, byte rate)
    {
        Intent intent = new Intent(/*fragment.getActivity()*/ Main.activity, PageNavigatorService.class);
        intent.putExtra(PageNavigatorService.INTENT_REFFERER, getPage());
        intent.putExtra(PageNavigatorService.INTENT_RATING_URL,ratingUrl);
        intent.putExtra(PageNavigatorService.INTENT_ID,id);
        intent.putExtra(PageNavigatorService.INTENT_TAGRZ,tagrz);
        intent.putExtra(PageNavigatorService.INTENT_POSITION,position);
        intent.putExtra(PageNavigatorService.INTENT_RATE,rate);
        intent.putExtra(PageNavigatorService.INTENT_TAG,PageNavigatorService.RATING_TAG);
        Main.activity.startService(intent);
    }

    /**
     * Set image resource url into local variable
     * @param src -url of image
     */
    public void     setImgSrc(String src)
    {
       imgSrc=src;
    }

    /**
     * get image url
     * @return - image url
     */
    public String   getImgSrc()
    {
       return imgSrc;
    }
    /**
     * Gets string value of current loaded page
     * @return - current loaded page string value
     */
    public String   getPage ()     {
        return CurrentPage;
    }

    /**
     * Set current page string value
     * @param Url - url of current page
     */
    private void    setPage    (String Url)
    {
        CurrentPage=Url;
    }

    /**
     * Get base page
     * @return
     */
    public String   getBasePage () {
        return BasePage;
    }

    /**
     * Set base page
     * @param Url
     */
    public void     setBasePage    (String Url)
    {
        BasePage=Url;
    }

    /**
     *
     * @return current page number
     */
    public int getCurrentPageNumber(){
        return currentPageNumber;
    }
    /**
     *
     * @return - maximum page number of current page type
     */
    public int getMaxPageNumber(){
        return maxPageNumber;
    }

    /**
     * Save max page number of current page type
     * @param maxNumber - maximum page number
     */
    public void setMaxPageNumber(int maxNumber) {
        maxPageNumber = maxNumber;
    }

    /**
     * Save current page number
     * @param number - current page number
     */
    public void setCurrentPageNumber(int number){
        currentPageNumber=number;
    }

    /**
     *
     * @return current page type
     */
    public byte getPageType(){
        return pageTagType;
    }

    /**
     * Save current page type
     * @param type - page category type
     */
    public void setPageType(byte type)
    {
        pageTagType=type;
        Main.activity.mainFragment.setTitle(type);
    }

    /**
     * Save max page number for category News
     * @param number - max page number for category news
     */
    public void setNewsPageMaxNumber(String number){
        newsPageMaxNumber=number;
    }

    /**
     *
     * @return - max page number for category News
     */
    public String getNewsPageMaxNumber(){
       return  newsPageMaxNumber;
    }

    /**
     * Init broadcast receiver which returns loaded data to activity
     */
    public void InitBroadcastReceiver() {
        // create BroadcastReceiver
         br = new BroadcastReceiver() {
            // receive massage
            public void onReceive(Context context, Intent intent) {
                byte tag = intent.getByteExtra(PageNavigatorService.INTENT_TAG, (byte) 1);
                Log.d("BROADCAST_RECEIVER_LOG", "onReceive");
                switch (tag) {
                    case PageNavigatorService.QUOTE_TAG:
                        Quotes = new Quote(intent.getBundleExtra(PageNavigatorService.INTENT_QUOTE));
                        setPage(intent.getStringExtra(PageNavigatorService.INTENT_CURRENT_PAGE));
                        currentPageNumber=Integer.valueOf(Quotes.getCurPageNum());
                        maxPageNumber=Quotes.getMaxPageNum();
                        Main.activity.mainFragment.onDataLoaded(Quotes);
                        break;
                    case PageNavigatorService.IMAGE_TAG:
                        setImgSrc(intent.getStringExtra(PageNavigatorService.INTENT_IMAGE_SRC));
                        setPage(intent.getStringExtra(PageNavigatorService.INTENT_CURRENT_PAGE));
                        Main.activity.mainFragment.onDataLoaded(Main.activity.mainFragment.IMAGE_SHOW,getImgSrc());
                        break;
                    case PageNavigatorService.NODATA_TAG:
                        Main.activity.mainFragment.onDataLoaded();
                        break;
                    case PageNavigatorService.RATING_TAG:
                        Main.activity.mainFragment.onPost(intent.getIntExtra(PageNavigatorService.INTENT_POSITION,0),intent.getByteExtra(PageNavigatorService.INTENT_RATE,(byte)0));
                    default:
                        break;
                }
            }

        };
        // create filter for BroadcastReceiver
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        // register BroadcastReceiver
        Main.activity.registerReceiver(br, intFilt);
    }

    /**
     * Save class data to SharedPreferences file
     */
    public void Save(){
        SharedPreferences sPref = Main.activity.getPreferences(Main.activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(SHARED_PREFERENCES_PN_CURRENT_PAGE,CurrentPage);
        ed.putString(SHARED_PREFERENCES_PN_BASE_PAGE,BasePage );
        ed.putInt(SHARED_PREFERENCES_PN_CURRENT_PAGE_NUMBER, currentPageNumber);
        ed.putInt(SHARED_PREFERENCES_PN_MAX_PAGE_NUMBER,maxPageNumber);
        ed.putInt(SHARED_PREFERENCES_PN_PAGE_TYPE,pageTagType);
        ed.putString(SHARED_PREFERENCES_PN_NEWS_PAGE_MAX_NUMBER, newsPageMaxNumber);
        ed.commit();
    }

    /**
     * Load data from SharedPreferences file to object properties
     */
    public void Load(){
        SharedPreferences sPref =Main.activity.getPreferences(Main.activity.MODE_PRIVATE);
        try {
            setPage(sPref.getString(SHARED_PREFERENCES_PN_CURRENT_PAGE, ""));
            setBasePage(sPref.getString(SHARED_PREFERENCES_PN_BASE_PAGE, ""));
            maxPageNumber=sPref.getInt(SHARED_PREFERENCES_PN_MAX_PAGE_NUMBER,0);
            currentPageNumber=sPref.getInt(SHARED_PREFERENCES_PN_CURRENT_PAGE_NUMBER,0);
            pageTagType=(byte)sPref.getInt(SHARED_PREFERENCES_PN_PAGE_TYPE,0);
            newsPageMaxNumber=sPref.getString(SHARED_PREFERENCES_PN_NEWS_PAGE_MAX_NUMBER,"0");
        }catch (Exception e){}
    }

    /**
     * Get category page string name by category number
     * @param number - category number
     * @return - String value with address of page
     */
    String getPageStringByNumber(byte number){
        String returnString="";
        switch (number){
            case  NEW_PAGE:         returnString=NEW_SPAGE;       break;
            case  RANDOM_PAGE:      returnString=RANDOM_SPAGE;    break;
            case  BEST_PAGE:        returnString=BEST_SPAGE;      break;
            case  RATING_PAGE:      returnString=RATING_SPAGE;    break;
            case  ABYSS_PAGE:       returnString=ABYSS_SPAGE;     break;
            case  ABYSS_TOP_PAGE:   returnString=ABYSSTOP_SPAGE;  break;
            case  ABYSS_BEST_PAGE:  returnString=ABBYSBEST_SPAGE; break;
            default: break;
        }
        return returnString;
    }
}
