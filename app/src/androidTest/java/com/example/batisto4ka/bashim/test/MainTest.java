package com.example.batisto4ka.bashim.test;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.test.ViewAsserts;
import android.util.Log;
import android.widget.ListView;

import com.example.batisto4ka.bashim.DB;
import com.example.batisto4ka.bashim.DBCach;
import com.example.batisto4ka.bashim.HtmlParseHelper;
import com.example.batisto4ka.bashim.Main;
import com.example.batisto4ka.bashim.PageNavigator;
import com.example.batisto4ka.bashim.PageNavigatorService;
import com.example.batisto4ka.bashim.Quote;
import com.example.batisto4ka.bashim.QuoteAdapter;
import com.example.batisto4ka.bashim.NavigationDrawerFragment;
import com.example.batisto4ka.bashim.PlaceHolderFragment;
import com.example.batisto4ka.bashim.R;

import org.jsoup.nodes.Document;

/**
 * Created by Batisto4ka on 12.10.2014.
 * Class that tests units of BashIm project
 */
public class MainTest extends ActivityInstrumentationTestCase2<Main> {
    /**
     * String value for null value
     */
    public static final String STRING__NULL=" is null";
    /**
     * String value for not null value
     */
    public static final String STRING_NOT_NULL=" is not null";
    /**
     * String value for Main test
     */
    public static final String STRING_MAIN_TEST="mMainTest";
    /**
     * Error log
     */
    public static final String LOG_CONNECTION_ERROR="CONNECTION_ERROR";
    /**
     * Reference on Main activity
     */
    private Main mMainTest;
    /**
     * Refrence on mainFragment in Main activity
     */
    PlaceHolderFragment mainFragment;
    /**
     * Reference on NavigationDrawerFragment with list og page categories
     */
    NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Reference on broadcast receiver
     */
    BroadcastReceiver br;
    /**
     * ListView with list of page content
     */
    ListView senderMassageListView;
    /**
     * ListView with list of page categories
     */
    ListView mDrawerListView;

    public MainTest() {
        super(Main.class);
    }

    /**
     * Initialize variables
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // Starts the activity under test using the default Intent with:
        // action = {@link Intent#ACTION_MAIN}
        // flags = {@link Intent#FLAG_ACTIVITY_NEW_TASK}
        // All other fields are null or empty.
        mMainTest = getActivity();
        mainFragment=getActivity().mainFragment;
        mNavigationDrawerFragment=getActivity().mNavigationDrawerFragment;
        br=mainFragment.pN.getReceiver();
        senderMassageListView=(ListView)mainFragment.getView().findViewById(R.id.listViewData);
        mDrawerListView=mNavigationDrawerFragment.mDrawerListView;
    }

    public void testCursor(){
        DB db=new DB(Main.activity);
        Cursor cursor=db.MainTable.ShowTable(DBCach.MAX_INDEX,PageNavigator.NEW_PAGE);
        int count=cursor.getCount();
        for(int j=0;j<count;j++)
        {
            assertTrue(cursor.moveToPosition(j));
        }

    }
    /**
     * Test if your test fixture has been set up correctly. You should always implement a test that
     * checks the correct setup of your test fixture. If this tests fails all other tests are
     * likely to fail as well.
     */
   public void testPreconditions() {
        //Try to add a message to add context to your assertions. These messages will be shown if
        //a tests fails and make it easy to understand why a test failed
        assertNotNull(STRING_MAIN_TEST+STRING_NOT_NULL, mMainTest);
        assertNotNull(mainFragment.toString()+STRING_NOT_NULL, mainFragment);
        assertNotNull(mNavigationDrawerFragment.toString()+STRING_NOT_NULL, mNavigationDrawerFragment);
        assertNotNull(senderMassageListView.toString()+STRING_NOT_NULL, senderMassageListView);
        assertNotNull(mDrawerListView.toString()+STRING_NOT_NULL,mDrawerListView);
    }

    /**
     * Test visibility of loader and pager of Activity
     */
    public void testControlsVisible() {
        ViewAsserts.assertOnScreen(mainFragment.loader.getRootView(), mainFragment.loader);
        ViewAsserts.assertOnScreen(mainFragment.pager.getRootView(),mainFragment.pager);
     }

    /**
     * Tests the screen orientation change
     */
    public void testOrientationChange(){
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        assertNotNull(mainFragment.toString()+STRING_NOT_NULL, mainFragment);
        assertNotNull(mainFragment.pN.toString()+STRING_NOT_NULL, mainFragment.pN);
        assertNotNull(mainFragment.pN.getReceiver().toString()+STRING_NOT_NULL, mainFragment.pN.getReceiver());
    }


    /**
     * Tests the broadcast receiver that gets quote data from intent
     */
  @UiThreadTest
   public void testBroadcast(){

        Intent intent = new Intent(PageNavigator.BROADCAST_ACTION);
        Quote quote=getTestQuote();
        intent.putExtra(PageNavigatorService.INTENT_QUOTE,quote.toBundle());
        intent.putExtra(PageNavigatorService.INTENT_CURRENT_PAGE,PageNavigator.BASE_PAGE);
        intent.putExtra(PageNavigatorService.INTENT_TAG, PageNavigatorService.QUOTE_TAG);
         br.onReceive(getActivity(), intent);
        assertEquals(PageNavigator.BASE_PAGE,mainFragment.pN.getPage());
        int expectedCount = 3;
        int actualCount = senderMassageListView.getAdapter().getCount();
        assertEquals(expectedCount, actualCount);
    }

    /**
     * Method that creates some example of quote object
     * @return - generated cQoute object
     */
    Quote getTestQuote(){
        Quote quote=new Quote(3);
        String[] quoteText={"Quote 1","Quote 2","Quote 3"};
        String[] quoteId={"#001","#002","#003"};
        String[] quoteRating={"8452","0","1478"};
        for(byte i=0;i<3;i++)
        {
            quote.setText(i,quoteText[i]);
            quote.setId(i,quoteId[i]);
            quote.setRating(0, quoteRating[i]);
        }
        quote.setCurPageNum("188");
        quote.setMaxPageNum(189);
        quote.setPageIndex((byte)3);
        quote.setPageType((byte)0);
        return quote;
    }

    /**
     * Tests HtmlParseHelper class with page category news
     */
    public void testHtmlParceHelper1(){
        htmlParseTestPageCategory(PageNavigator.NEW_PAGE);

    }
    /**
     * Tests HtmlParseHelper class with page category random
     */
    public void testHtmlParceHelper2(){
        htmlParseTestPageCategory(PageNavigator.RANDOM_PAGE);

    }
    /**
     * Tests HtmlParseHelper class with page category best
     */
    public void testHtmlParceHelper3(){
        htmlParseTestPageCategory(PageNavigator.BEST_PAGE);
    }
    /**
     * Tests HtmlParseHelper class with page category rating
     */
    public void testHtmlParceHelper4(){
        htmlParseTestPageCategory(PageNavigator.RATING_PAGE);
    }
    /**
     * Tests HtmlParseHelper class with page category abyss
     */
    public void testHtmlParceHelper5(){
        htmlParseTestPageCategory(PageNavigator.ABYSS_PAGE);
    }
    /**
     * Tests HtmlParseHelper class with page category abyss best
     */
    public void testHtmlParceHelper6(){
        htmlParseTestPageCategory(PageNavigator.ABYSS_BEST_PAGE);
    }
    /**
     * Tests HtmlParseHelper class with page category abyss top
     */
    public void testHtmlParceHelper7(){
        htmlParseTestPageCategory(PageNavigator.ABYSS_TOP_PAGE);
    }

    /**
     * Method that implements HtmlParseHelper class page parsing
     * @param type - page category type
     */
    void htmlParseTestPageCategory(byte type){

        Quote quote;
        String address;
        switch (type){
            case PageNavigator.NEW_PAGE:
                address=PageNavigator.BASE_PAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),false);
                assertEquals(quote.getMaxPageNum()==0,false);
                break;
            case PageNavigator.RANDOM_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.RANDOM_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),true);
                assertEquals(quote.getMaxPageNum(),0);
                break;
            case PageNavigator.BEST_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.BEST_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),true);
                assertEquals(quote.getMaxPageNum(),0);
                break;
         case PageNavigator.RATING_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.RATING_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),false);
                assertEquals(quote.getMaxPageNum()==0,false);
               break;
         case PageNavigator.ABYSS_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.ABYSS_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),true);
                assertEquals(quote.getMaxPageNum(),0);
                break;
         case PageNavigator.ABYSS_BEST_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.ABBYSBEST_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),true);
                assertEquals(quote.getMaxPageNum(),0);
                break;
        case PageNavigator.ABYSS_TOP_PAGE:
                address=PageNavigator.BASE_PAGE+PageNavigator.ABYSSTOP_SPAGE;
                quote=checkHtmlQuote(address);
                assertEquals((quote.getCurPageNum()).equals("0"),true);
                assertEquals(quote.getMaxPageNum(),0);
                break;
        }
    }

    /**
     * Methos that checks the returned quote for not null values
     * @param address - string value of http address of the page
     * @return quote - page parsed in quote object
     */
   Quote checkHtmlQuote(String address){
        HtmlParseHelper pH=new HtmlParseHelper();
        Document doc=pH.connect(address);
        Quote quote=null;
        if (doc!=null) {
            quote = pH.getData(doc, pH.QUOTE);
            assertNotNull(quote.toString()+STRING_NOT_NULL, quote);
            assertNotNull(quote.getText().toString()+STRING_NOT_NULL, quote.getText());
            assertNotNull(quote.getRating().toString()+STRING_NOT_NULL, quote.getRating());
            assertNotNull(quote.getCurPageNum().toString()+STRING_NOT_NULL, quote.getCurPageNum());
            assertNotNull(String.valueOf(quote.getMaxPageNum())+STRING_NOT_NULL, quote.getMaxPageNum());
        }
        else
            Log.e(LOG_CONNECTION_ERROR, address);
        return quote;
    }

    /**
     * Tests QuoteAdapter class
     */
    @UiThreadTest
    public void testQuoteAdapter(){
        Quote quote=getTestQuote();
        QuoteAdapter adapter=new QuoteAdapter(quote);
        senderMassageListView.setAdapter(adapter);
        int actualCount = senderMassageListView.getAdapter().getCount();
        assertEquals(quote.size(), actualCount);
    }

    /**
     * Tests BashIm database functionality
     * @throws Exception
     */
    public void testDataBase()throws Exception{
        DB db=new DB(Main.activity);
        Quote quote=getTestQuote();
        assertNotNull(db.toString()+STRING_NOT_NULL,db);
        assertNotNull(db.BashDB.toString()+STRING_NOT_NULL, db.BashDB);
        db.MainTable.Delete();
        db.MainTable.addRecords(quote);
        assertEquals(db.MainTable.CheckData(quote.getPageType(),Integer.valueOf(quote.getCurPageNum())),true);
        assertEquals(db.MainTable.CheckDataByIndex(quote.getPageIndex(),quote.getPageType()),true);
        db.MainTable.Delete(quote.getPageType(),Integer.valueOf(quote.getCurPageNum()));
        assertEquals(db.MainTable.CheckData(quote.getPageType(),Integer.valueOf(quote.getCurPageNum())),false);
    }

    /**
     * Tests DBCach class functionality
     * @throws Exception
     */
    public void testDBCach() throws Exception{
        String[] currentPageNumbers={"188","189","190"};
        byte[]   pageTypes={PageNavigator.NEW_PAGE,PageNavigator.RANDOM_PAGE,PageNavigator.RATING_PAGE};
        DBCach dbCach=new DBCach(Main.activity);
        //gererate quotes
        Quote quote=getTestQuote();
        //check db cach object
        assertNotNull(dbCach.db.toString()+STRING_NOT_NULL,dbCach.db);
        //delete all data from mainTable
        dbCach.db.MainTable.Delete();
        quote.setCurPageNum(currentPageNumbers[0]);
        quote.setPageType(pageTypes[0]);
        quote.setPageIndex(DBCach.MAX_INDEX);
        //put page in cach
        dbCach.putInCach(quote, quote.getPageType());
        //check current page with type page index in database, must be true
        assertEquals(dbCach.db.MainTable.CheckDataByIndex(DBCach.MAX_INDEX, quote.getPageType()), true);
        //check page index must be MAX_INDEX
        assertEquals(dbCach.db.MainTable.CheckIndexByTypeAndPageNumber(quote.getPageType(), quote.getCurPageNum(), DBCach.MAX_INDEX),true);
        //put another page to cach
        quote.setCurPageNum(currentPageNumbers[1]);
        quote.setPageType(pageTypes[1]);
        dbCach.putInCach(quote, quote.getPageType());
        //check current page index must be MAX_INDEX
        assertEquals(dbCach.db.MainTable.CheckDataByIndex(DBCach.MAX_INDEX, quote.getPageType()), true);
        //check previous page index must be MAX_INDEX-1
        assertEquals(dbCach.db.MainTable.CheckIndexByTypeAndPageNumber(pageTypes[0], currentPageNumbers[0], (byte) (DBCach.MAX_INDEX - 1)),true);
        //put another page in cach
        quote.setCurPageNum(currentPageNumbers[2]);
        quote.setPageType(pageTypes[2]);
        dbCach.putInCach(quote, quote.getPageType());
        //check current page index must be MAX_INDEX
        assertEquals(dbCach.db.MainTable.CheckDataByIndex(DBCach.MAX_INDEX, quote.getPageType()), true);
        //check previous page index must be MAX_INDEX-1
        assertEquals(dbCach.db.MainTable.CheckIndexByTypeAndPageNumber(pageTypes[0], currentPageNumbers[0], (byte) (DBCach.MAX_INDEX - 2)),true);
        ////check preprevious page index must be MAX_INDEX-2
        assertEquals(dbCach.db.MainTable.CheckIndexByTypeAndPageNumber(pageTypes[1],currentPageNumbers[1], (byte) (DBCach.MAX_INDEX-1)),true);
    }
}
